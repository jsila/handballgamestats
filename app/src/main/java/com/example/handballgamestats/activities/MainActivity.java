package com.example.handballgamestats.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.handballgamestats.R;
import com.example.handballgamestats.dialogs.AddGameInfoDialogFragment;
import com.example.handballgamestats.dialogs.SelectStatDialogFragment;
import com.example.handballgamestats.lib.Csv;
import com.example.handballgamestats.lib.GameInfo;
import com.example.handballgamestats.lib.Player;
import com.example.handballgamestats.lib.Players;
import com.example.handballgamestats.lib.Stats;
import com.example.handballgamestats.lib.StatsDbHelper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements
        SelectStatDialogFragment.SelectStatDialogListener,
        AddGameInfoDialogFragment.AddGameInfoDialogListener {

    public static final int PICK_FILE = 1;

    public static final String EXTRA_PLAYERS = "EXTRA_PLAYERS";
    public static final String EXTRA_FILE_URI = "EXTRA_FILE_URI";
    public static final String EXTRA_ONLY_GKS = "EXTRA_ONLY_GKS";

    protected Players players;

    PlayerAdapter adapterGKs;
    PlayerAdapter adapterRest;

    TextView gameScoreText;
    TextView gameScore;

    GameInfo gameInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onResume() {
        super.onResume();

        players = new Players(StatsDbHelper.getInstance(this));

        adapterGKs = new PlayerAdapter(getBaseContext(), R.layout.activity_main_list_item, players.getPlayersGKs());
        ListView listViewGKs = (ListView) findViewById(R.id.list_gks);
        listViewGKs.setAdapter(adapterGKs);

        adapterRest = new PlayerAdapter(getBaseContext(), R.layout.activity_main_list_item, players.getPlayersRest());
        ListView listViewRest = (ListView) findViewById(R.id.list_rest);
        listViewRest.setAdapter(adapterRest);

        listViewGKs.setOnItemClickListener(setOnItemClickListener(players.getPlayersGKs()));
        listViewGKs.setOnItemLongClickListener(onItemLonClickListener(players.getPlayersGKs()));

        listViewRest.setOnItemClickListener(setOnItemClickListener(players.getPlayersRest()));
        listViewRest.setOnItemLongClickListener(onItemLonClickListener(players.getPlayersRest()));


        gameScore = (TextView) findViewById(R.id.game_score);
        gameScoreText = (TextView) findViewById(R.id.game_score_text);

        gameInfo = StatsDbHelper.getInstance(this).getGameInfo();
        gameScoreText.setText(gameInfo.getGameScoreText());

        updateGameScore();
    }

    @NonNull
    private AdapterView.OnItemLongClickListener onItemLonClickListener(final ArrayList<Player> players1) {
        return new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                setOnItemClickListenerType(i, false, players1);
                return true;
            }
        };
    }

    @NonNull
    private AdapterView.OnItemClickListener setOnItemClickListener(final ArrayList<Player> players1) {
        return new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                setOnItemClickListenerType(i, true, players1);
            }
        };
    }

    private void setOnItemClickListenerType(int i, boolean isAdd, ArrayList<Player> players1) {
        Player player = players1.get(i);

        Bundle args = new Bundle();

        args.putBoolean("isGKDialog", player.isGoalkeeper());
        args.putBoolean("isGKPlayer", player.isAssociatedPlayer());
        args.putLong("playerId", player.getId());
        args.putString("playerName", player.getName());
        args.putBoolean("isAdd", isAdd);

        DialogFragment fragment = new SelectStatDialogFragment();
        fragment.setArguments(args);
        fragment.show(getSupportFragmentManager(), "SelectStatDialogFragment");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.new_game:
                startNewGameActivity(); break;
            case R.id.view_game:
                startViewGameActivity(); break;
            case R.id.export_game:
                exportGame(); break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void startViewGameActivity() {
        Intent intent = new Intent(getApplicationContext(), OverviewActivity.class);
        startActivity(intent);
    }

    private void startNewGameActivity() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("text/plain");
        startActivityForResult(intent, PICK_FILE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case PICK_FILE:
                onPickFileActivityResult(data);
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void onPickFileActivityResult(Intent data) {
        ArrayList<String> players = new ArrayList<>();
        ArrayList<String> onlyGks = new ArrayList<>();

        try {
            // if user doesn't select a file
            if (data == null) {
                return;
            }
            BufferedReader reader = new BufferedReader(new InputStreamReader(getContentResolver().openInputStream(data.getData())));
            String line;

            while ((line = reader.readLine()) != null) {
                String playerLine = line.trim();
                if (playerLine.isEmpty()) continue;

                if (playerLine.contains(",G")) {
                    playerLine = playerLine.split(",")[0].trim();
                    onlyGks.add(playerLine);
                }
                players.add(playerLine);
            }
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Težave z branjem datoteke. Poskusi ponovno.", Toast.LENGTH_LONG).show();
            return;
        }

        Intent intent = new Intent(this, PlayerListActivity.class);
        intent.putExtra(EXTRA_PLAYERS, players);
        intent.putExtra(EXTRA_ONLY_GKS, onlyGks);
        intent.putExtra(EXTRA_FILE_URI, data.getData());
        startActivity(intent);
    }

    public void exportGame() {

        if(!isExternalStorageWritable()) {
            Toast.makeText(this, "Ni možno shraniti", Toast.LENGTH_SHORT).show();
            return;
        }

        Bundle args = new Bundle();
        args.putParcelable(GameInfo.class.getName(), gameInfo);

        DialogFragment fragment = new AddGameInfoDialogFragment();
        fragment.setArguments(args);
        fragment.show(getSupportFragmentManager(), "AddGameInfoDialogFragment");
    }

    @Override
    public void onSelectStatClick(String playerName, boolean isGK, int statId, int amount) {
        updateStat(
                isGK ? players.getPlayersGKs() : players.getPlayersRest(),
                playerName,
                statId,
                amount
        );

        adapterGKs.notifyDataSetChanged();
        adapterRest.notifyDataSetChanged();
        updateGameScore();
    }

    public void updateStat(ArrayList<Player> players, String playerName, int statId, int amount) {
        for (int i = 0; i < players.size(); i++) {
            if(!players.get(i).getName().equals(playerName)) continue;

            players.get(i).setScore(statId, players.get(i).getScore(statId) + amount);

            StatsDbHelper.getInstance(this).updateStat(statId, players.get(i));
            break;
        }
    }

    private void updateGameScore() {
        int goalsAgainst = Stats.getGoalsAgainst(players.getPlayersMap()); // other team
        int goalsFor = Stats.getGoalsFor(players.getPlayersMap()); // user team

        gameInfo.setScore(goalsFor, goalsAgainst);
        gameScore.setText(gameInfo.getScore());
    }

    @Override
    public void onAddGameInfoDialogClick(GameInfo info) {
        if (players.getPlayersGKs().size() == 0 || players.getPlayersRest().size() == 0) {
            Toast.makeText(this, "Izvoz ni mogoč, ker ni na voljo nobenih podatkov", Toast.LENGTH_SHORT).show();
            return;
        }

        ArrayList<Player> playersRestPlayers = new ArrayList<>();
        ArrayList<Player> playersRestGKS = new ArrayList<>();

        for (Player player: players.getPlayersRest()) {
            if (player.isAssociatedPlayer()) {
                playersRestGKS.add(player);
            } else {
                playersRestPlayers.add(player);
            }
        }

        Csv csv = new Csv(35, 40);

        csv.addGKsHeader(                                         new Csv.StartAt(0, 0));
        csv.addPayersData(players.getPlayersGKs(),                new Csv.StartAt(3, 1), players.getPlayersMap());
        csv.addSummary("Skupaj Golmani", players.getPlayersGKs(), new Csv.StartAt(5, 2 + players.playersGKsQTY()));
        csv.addSummary("Skupaj", players.getPlayersRest(),        new Csv.StartAt(5, 3 + players.playersGKsQTY()));
        csv.addPlayersHeader(                                     new Csv.StartAt(3, 4 + players.playersGKsQTY()));
        csv.addPayersData(playersRestPlayers,                     new Csv.StartAt(3, 5 + players.playersGKsQTY()), players.getPlayersMap());
        csv.addGKSPlayersHeader(                                  new Csv.StartAt(0, 11 + players.playersGKsQTY() + players.playersRestQTY() - players.GKsPlayersQTY()));
        csv.addGKSPlayersData(playersRestGKS,                     new Csv.StartAt(4, 12 + players.playersGKsQTY() + players.playersRestQTY() - players.GKsPlayersQTY()));
        csv.addGameInfo(info,                                     new Csv.StartAt(0, 8));

        String filename = info.getGameScoreText().replace(" : ", "-") + "-" + info.getDate() + ".csv";
        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), filename);

        try {
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(csv.getResult().getBytes());
            fos.close();
        } catch (Exception e) {
            Toast.makeText(this, "Problem pisanja v datoteko", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
        intent.setType("text/csv");
        startActivity(intent);
    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    public class PlayerAdapter extends ArrayAdapter<Player> {
        PlayerAdapter(Context context, int id, ArrayList<Player> players) {
            super(context, id, players);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Player player = getItem(position);
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.activity_main_list_item, parent, false);
            }

            TextView name       = (TextView) convertView.findViewById(R.id.name);
            TextView min2       = (TextView) convertView.findViewById(R.id.min2);
            TextView strike     = (TextView) convertView.findViewById(R.id.strike);
            TextView goalRation = (TextView) convertView.findViewById(R.id.goal_ration);
            TextView index      = (TextView) convertView.findViewById(R.id.index);

            name.setText(player.getName());
            goalRation.setText(String.format("%.2f", player.getStats().getGoalRatio()));

            if (player.isAssociatedPlayer()) {
                min2.setText(" ");
                strike.setText(" ");
                index.setText(" ");
            } else {
                min2.setText(String.format("%d", player.getScore(Stats.PENALTY_2MIN)));
                strike.setText(String.valueOf(player.getScore(Stats.STRIKE)));
                index.setText(String.valueOf(player.getStats().getIndex(players.getPlayersMap())));
            }

            return convertView;
        }
    }
}