package com.example.handballgamestats.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TableLayout;
import android.widget.TableRow;

import com.example.handballgamestats.R;
import com.example.handballgamestats.lib.Player;
import com.example.handballgamestats.lib.Players;
import com.example.handballgamestats.lib.Stats;
import com.example.handballgamestats.lib.StatsDbHelper;
import com.example.handballgamestats.widgets.TextView;

import java.util.ArrayList;
import java.util.Map;

public class OverviewActivity extends AppCompatActivity {

    StatsDbHelper statsDb;

    protected Players playersData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_overview);
        getSupportActionBar().setTitle("Celotna statistika");
    }

    @Override
    protected void onResume() {
        super.onResume();
        statsDb = StatsDbHelper.getInstance(this);

        playersData = new Players(statsDb);

        TableLayout table = (TableLayout)findViewById(R.id.table_overview);
        TableRow row;

        row = (TableRow) getLayoutInflater().inflate(R.layout.activity_overview_gks_tablerow_header, null);
        table.addView(row);
        addPlayersRows(table, playersData.getPlayersGKs());

        addPlayersSumRows(table, playersData.getPlayersGKs(), "Skupaj (golmani)");
        addPlayersSumRows(table, playersData.getPlayersRest(), "Skupaj");

        row = (TableRow) getLayoutInflater().inflate(R.layout.activity_overview_rest_tablerow_header, null);
        table.addView(row);
        addPlayersRows(table, playersData.getPlayersRest());

    }

    private void addPlayersSumRows(TableLayout table, ArrayList<Player> players, String rowTitle) {
        Map<Integer, Integer> scores = Stats.sumScores(players);

        TableRow row = new TableRow(getBaseContext());

        TextView r1 = new TextView(getBaseContext());
        r1.setAsBold();
        r1.markAsFirstColumn();
        r1.setText(rowTitle);
        row.addView(r1);

        TextView r2 = new TextView(getBaseContext());
        r2.setAsBold();
        r2.setIntegerText(scores.get(Stats.GOAL_PLUS));
        row.addView(r2);

        TextView r3 = new TextView(getBaseContext());
        r3.setAsBold();
        r3.setDoubleText(Stats.getGoalRatio(scores));
        row.addView(r3);

        TextView r4 = new TextView(getBaseContext());
        r4.setAsBold();
        r4.setIntegerText(scores.get(Stats.GAME_GOAL_PLUS));
        row.addView(r4);

        TextView r5 = new TextView(getBaseContext());
        r5.setAsBold();
        r5.setIntegerText(scores.get(Stats.GAME_GOAL_MINUS));
        row.addView(r5);

        TextView r6 = new TextView(getBaseContext());
        r6.setAsBold();
        r6.setText(Stats.getGameGoalsRatioString(scores));
        row.addView(r6);

        TextView r7 = new TextView(getBaseContext());
        r7.setAsBold();
        r7.setDoubleText(Stats.getGameGoalsRatio(scores));
        row.addView(r7);

        TextView r22 = new TextView(getBaseContext());
        r22.setAsBold();
        r22.setIntegerText(scores.get(Stats.GOAL_9M_PLUS));
        row.addView(r22);

        TextView r23 = new TextView(getBaseContext());
        r23.setAsBold();
        r23.setIntegerText(scores.get(Stats.GOAL_9M_MINUS));
        row.addView(r23);

        TextView r24 = new TextView(getBaseContext());
        r24.setAsBold();
        r24.setIntegerText(scores.get(Stats.GOAL_6M_PLUS));
        row.addView(r24);

        TextView r25 = new TextView(getBaseContext());
        r25.setAsBold();
        r25.setIntegerText(scores.get(Stats.GOAL_6M_MINUS));
        row.addView(r25);

        TextView r26 = new TextView(getBaseContext());
        r26.setAsBold();
        r26.setIntegerText(scores.get(Stats.GOAL_COUNTERATTACK_PLUS));
        row.addView(r26);

        TextView r27 = new TextView(getBaseContext());
        r27.setAsBold();
        r27.setIntegerText(scores.get(Stats.GOAL_COUNTERATTACK_MINUS));
        row.addView(r27);

        TextView r8 = new TextView(getBaseContext());
        r8.setAsBold();
        r8.setIntegerText(scores.get(Stats.GOAL_7M_PLUS));
        row.addView(r8);

        TextView r9 = new TextView(getBaseContext());
        r9.setAsBold();
        r9.setIntegerText(scores.get(Stats.GOAL_7M_MINUS));
        row.addView(r9);

        TextView r10 = new TextView(getBaseContext());
        r10.setAsBold();
        r10.setText(Stats.get7MGoalsRatioString(scores));
        row.addView(r10);

        TextView r11 = new TextView(getBaseContext());
        r11.setAsBold();
        r11.setDoubleText(Stats.get7MGoalsRatio(scores));
        row.addView(r11);

        TextView r12 = new TextView(getBaseContext());
        r12.setAsBold();
        r12.setIntegerText(scores.get(Stats.ASSIST));
        row.addView(r12);

        TextView r13 = new TextView(getBaseContext());
        r13.setAsBold();
        r13.setIntegerText(scores.get(Stats.BALLS_LOST));
        row.addView(r13);

        TextView r14 = new TextView(getBaseContext());
        r14.setAsBold();
        r14.setIntegerText(scores.get(Stats.BALLS_WON));
        row.addView(r14);

        TextView r15 = new TextView(getBaseContext());
        r15.setAsBold();
        r15.setIntegerText(scores.get(Stats.BALLS_7M_WON));
        row.addView(r15);

        TextView r28 = new TextView(getBaseContext());
        r28.setAsBold();
        r28.setIntegerText(scores.get(Stats.BALLS_7M_GIVEAWAY));
        row.addView(r28);

        TextView r16 = new TextView(getBaseContext());
        r16.setAsBold();
        r16.setIntegerText(scores.get(Stats.BLOCK));
        row.addView(r16);

        TextView r17 = new TextView(getBaseContext());
        r17.setAsBold();
        r17.setIntegerText(scores.get(Stats.STRIKE));
        row.addView(r17);

        TextView r29 = new TextView(getBaseContext());
        r29.setAsBold();
        r29.setIntegerText(scores.get(Stats.STRIKE_PLUS));
        row.addView(r29);

        TextView r18 = new TextView(getBaseContext());
        r18.setAsBold();
        r18.setIntegerText(scores.get(Stats.PENALTY_2MIN));
        row.addView(r18);

        TextView r99 = new TextView(getBaseContext());
        r99.setAsBold();
        r99.setIntegerText(scores.get(Stats.PENALTY_2MIN_WON));
        row.addView(r99);

        TextView r19 = new TextView(getBaseContext());
        r19.setAsBold();
        r19.setIntegerText(scores.get(Stats.RED_CARD));
        row.addView(r19);

        TextView r20 = new TextView(getBaseContext());
        r20.setAsBold();
        r20.setIntegerText(scores.get(Stats.FOUL));
        row.addView(r20);

        table.addView(row);
    }

    private void addPlayersRows(TableLayout table, ArrayList<Player> players) {
        for(Player player: players) {
            TableRow row = new TableRow(getBaseContext());

            TextView r1 = new TextView(getBaseContext());
            r1.markAsFirstColumn();
            r1.setText(player.getName());
            row.addView(r1);

            TextView r2 = new TextView(getBaseContext());
            r2.setIntegerText(player.getStats().getScore(Stats.GOAL_PLUS));
            row.addView(r2);

            TextView r3 = new TextView(getBaseContext());
            r3.setDoubleText(player.getStats().getGoalRatio());
            row.addView(r3);

            TextView r4 = new TextView(getBaseContext());
            r4.setIntegerText(player.getStats().getScore(Stats.GAME_GOAL_PLUS));
            row.addView(r4);

            TextView r5 = new TextView(getBaseContext());
            r5.setIntegerText(player.getStats().getScore(Stats.GAME_GOAL_MINUS));
            row.addView(r5);

            TextView r6 = new TextView(getBaseContext());
            r6.setText(player.getStats().getGameGoalsRatioString());
            row.addView(r6);

            TextView r7 = new TextView(getBaseContext());
            r7.setDoubleText(player.getStats().getGameGoalsRatio());
            row.addView(r7);

            TextView r22 = new TextView(getBaseContext());
            r22.setIntegerText(player.getStats().getScore(Stats.GOAL_9M_PLUS));
            row.addView(r22);

            TextView r23 = new TextView(getBaseContext());
            r23.setIntegerText(player.getStats().getScore(Stats.GOAL_9M_MINUS));
            row.addView(r23);

            TextView r24 = new TextView(getBaseContext());
            r24.setIntegerText(player.getStats().getScore(Stats.GOAL_6M_PLUS));
            row.addView(r24);

            TextView r25 = new TextView(getBaseContext());
            r25.setIntegerText(player.getStats().getScore(Stats.GOAL_6M_MINUS));
            row.addView(r25);

            TextView r26 = new TextView(getBaseContext());
            r26.setIntegerText(player.getStats().getScore(Stats.GOAL_COUNTERATTACK_PLUS));
            row.addView(r26);

            TextView r27 = new TextView(getBaseContext());
            r27.setIntegerText(player.getStats().getScore(Stats.GOAL_COUNTERATTACK_MINUS));
            row.addView(r27);

            TextView r8 = new TextView(getBaseContext());
            r8.setIntegerText(player.getStats().getScore(Stats.GOAL_7M_PLUS));
            row.addView(r8);

            TextView r9 = new TextView(getBaseContext());
            r9.setIntegerText(player.getStats().getScore(Stats.GOAL_7M_MINUS));
            row.addView(r9);

            TextView r10 = new TextView(getBaseContext());
            r10.setText(player.getStats().get7MGoalsRatioString());
            row.addView(r10);

            TextView r11 = new TextView(getBaseContext());
            r11.setDoubleText(player.getStats().get7MGoalsRatio());
            row.addView(r11);

            TextView r12 = new TextView(getBaseContext());
            r12.setIntegerText(player.getStats().getScore(Stats.ASSIST));
            row.addView(r12);

            TextView r13 = new TextView(getBaseContext());
            r13.setIntegerText(player.getStats().getScore(Stats.BALLS_LOST));
            row.addView(r13);

            TextView r14 = new TextView(getBaseContext());
            r14.setIntegerText(player.getStats().getScore(Stats.BALLS_WON));
            row.addView(r14);

            TextView r15 = new TextView(getBaseContext());
            r15.setIntegerText(player.getStats().getScore(Stats.BALLS_7M_WON));
            row.addView(r15);

            TextView r28 = new TextView(getBaseContext());
            r28.setAsBold();
            r28.setIntegerText(player.getStats().getScore(Stats.BALLS_7M_GIVEAWAY));
            row.addView(r28);

            TextView r16 = new TextView(getBaseContext());
            r16.setIntegerText(player.getStats().getScore(Stats.BLOCK));
            row.addView(r16);

            TextView r17 = new TextView(getBaseContext());
            r17.setIntegerText(player.getStats().getScore(Stats.STRIKE));
            row.addView(r17);

            TextView r33 = new TextView(getBaseContext());
            r33.setIntegerText(player.getStats().getScore(Stats.STRIKE_PLUS));
            row.addView(r33);

            TextView r18 = new TextView(getBaseContext());
            r18.setIntegerText(player.getStats().getScore(Stats.PENALTY_2MIN));
            row.addView(r18);

            TextView r29 = new TextView(getBaseContext());
            r29.setAsBold();
            r29.setIntegerText(player.getStats().getScore(Stats.PENALTY_2MIN_WON));
            row.addView(r29);

            TextView r19 = new TextView(getBaseContext());
            r19.setIntegerText(player.getStats().getScore(Stats.RED_CARD));
            row.addView(r19);

            TextView r20 = new TextView(getBaseContext());
            r20.setIntegerText(player.getStats().getScore(Stats.FOUL));
            row.addView(r20);

            TextView r21 = new TextView(getBaseContext());
            if (player.isAssociatedPlayer()) {
                r21.setText(" ");
            } else {
                r21.setIntegerText(player.getStats().getIndex(playersData.getPlayersMap()));
            }
            row.addView(r21);

            table.addView(row);
        }
    }
}
