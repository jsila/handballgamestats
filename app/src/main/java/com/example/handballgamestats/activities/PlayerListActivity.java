package com.example.handballgamestats.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.handballgamestats.R;
import com.example.handballgamestats.adapters.PlayerListAdapter;
import com.example.handballgamestats.dialogs.AddPlayerDialogFragment;
import com.example.handballgamestats.dialogs.ConfirmPlayerSelectionDialogFragment;
import com.example.handballgamestats.lib.StatsDbHelper;
import com.example.handballgamestats.lib.Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class PlayerListActivity extends AppCompatActivity implements AddPlayerDialogFragment.AddPlayerDialogListener, ConfirmPlayerSelectionDialogFragment.ConfirmPlayerSelectionDialogListener {
    public static final int CONFIRM_DELETE_SELECTION = 1;
    public static final int CONFIRM_GAME_SELECTION = 2;

    private ArrayList<String> players;
    private PlayerListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_list);
        getSupportActionBar().setTitle("Izbira ekipe");

        Intent intent = getIntent();
        players = intent.getStringArrayListExtra(MainActivity.EXTRA_PLAYERS);
        ArrayList<String> onlyGKs = intent.getStringArrayListExtra(MainActivity.EXTRA_ONLY_GKS);

        Utils.sortStringArrayList(players);

        setupListView();

        adapter.preselectPlayersAndGKs(players, onlyGKs);
    }

    private void setupListView() {
        adapter = new PlayerListAdapter(getBaseContext(), android.R.layout.simple_list_item_1, players);
        final ListView listView = (ListView) findViewById(R.id.player_listview);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                adapter.selectPlayer(players.get(i));
                setSupportActionBarTitle();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.playerlist_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_player:
                onAddPlayerOptionItemSelected();
                break;
            case R.id.save_list:
                onSaveListOptionItemSelected();
                break;
            case R.id.select_players:
                onSelectPlayersOptionItemSelected();
                break;
            case R.id.select_goalkeepers:
                onSelectGoalkeepersOptionItemSelected();
                break;
            case R.id.delete_players:
                onDeletePlayersOptionItemSelected();
                break;
            case R.id.start_game:
                onStartGameOptionItemSelected();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void onStartGameOptionItemSelected() {
        ArrayList<String> selected = new ArrayList<>();

        selected.addAll(adapter.getSelectedGKs());
        Utils.sortStringArrayList(selected);
        ArrayList<String> other = Utils.removeDuplicates(adapter.getSelectedRest(), adapter.getSelectedGKs());
        Utils.sortStringArrayList(other);
        selected.addAll(other);

        for (int i=0; i<adapter.getSelectedGKs().size(); i++) {
            selected.set(i, selected.get(i) + " (G)");
        }

        Bundle args = new Bundle();
        args.putStringArrayList("players", selected);
        args.putInt("action", CONFIRM_GAME_SELECTION);

        DialogFragment fragment2 = new ConfirmPlayerSelectionDialogFragment();
        fragment2.setArguments(args);
        fragment2.show(getSupportFragmentManager(), "ConfirmGamePlayerSelectionDialogFragment");
    }

    private void onDeletePlayersOptionItemSelected() {
        if (adapter.isMode(PlayerListAdapter.DELETE_MODE)) {
            adapter.turnOffMode();
            setSupportActionBarTitle();

            Bundle args = new Bundle();
            args.putStringArrayList("players", adapter.getSelectedForDelete());
            args.putInt("action", CONFIRM_DELETE_SELECTION);

            DialogFragment fragment2 = new ConfirmPlayerSelectionDialogFragment();
            fragment2.setArguments(args);
            fragment2.show(getSupportFragmentManager(), "ConfirmDeletePlayerSelectionDialogFragment");
        } else {
            adapter.switchToDeleteMode();
            setSupportActionBarTitle();
            Toast.makeText(getApplicationContext(), "Izbira igralcev za odstranitev s seznama", Toast.LENGTH_LONG).show();
        }
    }

    private void onSelectGoalkeepersOptionItemSelected() {
        adapter.switchToGoalkeepersMode();
        setSupportActionBarTitle();
        Toast.makeText(getApplicationContext(), "Izbira aktivnih golmanov", Toast.LENGTH_LONG).show();
    }

    private void onSelectPlayersOptionItemSelected() {
        adapter.switchToPlayersMode();
        setSupportActionBarTitle();
        Toast.makeText(getApplicationContext(), "Izbira aktivnih igralcev", Toast.LENGTH_LONG).show();
    }

    private void setSupportActionBarTitle() {
        String title;
        String subtitle = String.format("%s izbranih", adapter.getSelectedModeSize());

        switch (adapter.getMode()) {
            case PlayerListAdapter.DELETE_MODE:
                title = "Izbris igralcev s seznama";
                break;
            case PlayerListAdapter.SELECT_PLAYERS_MODE:
                title = "Aktivni igralci";
                break;
            case PlayerListAdapter.SELECT_GOALKEEPERS_MODE:
                title = "Aktvini golmani";
                break;
            default:
                title = "Izbira ekipe";
                subtitle = null;
        }

        getSupportActionBar().setTitle(title);
        getSupportActionBar().setSubtitle(subtitle);

    }

    private void onSaveListOptionItemSelected() {
        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), "players.txt");
        Utils.sortStringArrayList(players);

        try {
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(TextUtils.join("\n", adapter.collectPlayersForListSave()).getBytes());
            fos.close();
        } catch (Exception e) {
            Toast.makeText(this, "Problem pisanja v datoteko", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
        intent.setType("text/plain");
        startActivity(intent);
    }

    private void onAddPlayerOptionItemSelected() {
        DialogFragment fragment1 = new AddPlayerDialogFragment();
        fragment1.show(getSupportFragmentManager(), "AddPlayerDialogFragment");
    }

    @Override
    public void onAddPlayerDialogClick(String playerName, boolean isGoalkeeper) {
        players.add(playerName);
        adapter.selectPlayer(playerName, isGoalkeeper);
        Utils.sortStringArrayList(players);
        adapter.notifyDataSetChanged();

        Toast.makeText(getApplicationContext(), String.format("Dodan %s %s", isGoalkeeper ? "vratar" : "igralec", playerName), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConfirmDeletePlayerSelection() {
        ArrayList<String> toDelete = adapter.getSelectedForDelete();
        adapter.turnOffMode();
        players.removeAll(toDelete);
        adapter.clearToDeleteSelection();
        Toast.makeText(getApplicationContext(), String.format("%d igralcev odstranjenih", toDelete.size()), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConfirmGamePlayerSelection(String userTeamName, String otherTeamName, boolean isGuestGame) {
        StatsDbHelper statsDb = StatsDbHelper.getInstance(this);

        statsDb.recreateDB();
        statsDb.savePlayers(
                adapter.getSelectedGKs(),
                // in case gks are in rest too, if player is gks than he is not rest
                Utils.removeDuplicates(adapter.getSelectedRest(), adapter.getSelectedGKs())
        );
        statsDb.saveInfo(userTeamName, otherTeamName, isGuestGame);

        finish();
    }

    @Override
    public void onBackPressed() {
        if (adapter.isOnMode()) {
            adapter.turnOffMode();
            setSupportActionBarTitle();
            Toast.makeText(getApplicationContext(), "Konec izbiranja", Toast.LENGTH_SHORT).show();
        } else {
            super.onBackPressed();
        }
    }
}
