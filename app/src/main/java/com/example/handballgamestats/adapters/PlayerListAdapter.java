package com.example.handballgamestats.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.TextView;

import com.example.handballgamestats.R;
import com.example.handballgamestats.lib.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PlayerListAdapter extends ArrayAdapter<String> {
    public static final int NO_MODE = 0;
    public static final int SELECT_GOALKEEPERS_MODE = 1;
    public static final int SELECT_PLAYERS_MODE = 2;
    public static final int DELETE_MODE = 3;

    private Map<String, Boolean> deleteSelection;
    private Map<String, Boolean> playersGKsSelection;
    private Map<String, Boolean> playersRestSelection;

    private int currentMode;

    public PlayerListAdapter(Context context, int id, ArrayList<String> players) {
        super(context, id, players);
        this.deleteSelection = new HashMap<>();
        this.playersGKsSelection = new HashMap<>();
        this.playersRestSelection = new HashMap<>();

        this.currentMode = 0;
    }

    public boolean isMode(int mode) {
        return this.currentMode == mode;
    }

    public int getMode() {
        return this.currentMode;
    }

    public void switchToPlayersMode() {
        this.currentMode = SELECT_PLAYERS_MODE;
        notifyDataSetChanged();
    }

    public void switchToGoalkeepersMode() {
        this.currentMode = SELECT_GOALKEEPERS_MODE;
        notifyDataSetChanged();
    }

    public void switchToDeleteMode() {
        this.currentMode = DELETE_MODE;
        notifyDataSetChanged();
    }

    public void turnOffMode() {
        this.currentMode = NO_MODE;
        notifyDataSetChanged();
    }

    public boolean isOnMode() {
        return this.currentMode > 0;
    }

    public ArrayList<String> getSelectedGKs() {
        return new ArrayList<>(this.playersGKsSelection.keySet());
    }

    public ArrayList<String> getSelectedRest() {
        return new ArrayList<>(this.playersRestSelection.keySet());
    }

    public ArrayList<String> getSelectedForDelete() {
        return new ArrayList<>(this.deleteSelection.keySet());
    }

    public int getSelectedModeSize() {
        switch(this.currentMode) {
            case SELECT_PLAYERS_MODE:
                return this.getSelectedRest().size();
            case SELECT_GOALKEEPERS_MODE:
                return this.getSelectedGKs().size();
            case DELETE_MODE:
                return this.getSelectedForDelete().size();
            default:
                return 0;
        }

    }

    public void clearToDeleteSelection() {
        this.deleteSelection.clear();
        notifyDataSetChanged();
    }

    public void selectPlayer(String playerName) {
        switch(this.currentMode) {
            case SELECT_PLAYERS_MODE:
                selectPlayerInSelection(playersRestSelection, playerName);
                break;
            case SELECT_GOALKEEPERS_MODE:
                selectPlayerInSelection(playersGKsSelection, playerName);
                break;
            case DELETE_MODE:
                selectPlayerInSelection(deleteSelection, playerName);
                break;
            default:
                return;
        }
        notifyDataSetChanged();
    }

    public void selectPlayer(String playerName, boolean isGK) {
        if (isGK) {
            selectPlayerInSelection(playersGKsSelection, playerName);
        } else {
            selectPlayerInSelection(playersRestSelection, playerName);
        }
    }

    private void selectPlayerInSelection(Map<String, Boolean> sel, String playerName) {
        if (sel.containsKey(playerName)) {
            sel.remove(playerName);
        } else {
            sel.put(playerName, true);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String playerName = getItem(position);
        View v;
        if (this.isOnMode()) {
            v = LayoutInflater.from(getContext()).inflate(R.layout.activity_player_list_item_checked, parent, false);
            CheckedTextView textView = (CheckedTextView) v.findViewById(android.R.id.text1);
            textView.setText(playerName);
            switch(this.currentMode) {
                case SELECT_PLAYERS_MODE:
                    textView.setChecked(this.playersRestSelection.containsKey(playerName));
                    break;
                case SELECT_GOALKEEPERS_MODE:
                    textView.setChecked(this.playersGKsSelection.containsKey(playerName));
                    break;
                case DELETE_MODE:
                    textView.setChecked(this.deleteSelection.containsKey(playerName));
                    break;
            }
        } else {
            v = LayoutInflater.from(getContext()).inflate(R.layout.activity_player_list_item, parent, false);
            TextView textView = (TextView) v.findViewById(android.R.id.text1);
            textView.setText(playerName);
        }

        return v;
    }

    public void preselectPlayersAndGKs(ArrayList<String> players, ArrayList<String> onlyGKs) {
        playersRestSelection.clear();
        playersGKsSelection.clear();
        for (String player: players) {
            selectPlayerInSelection(
                    onlyGKs.contains(player) ? playersGKsSelection : playersRestSelection,
                    player
            );
        }
    }

    public ArrayList<String> collectPlayersForListSave() {
        ArrayList<String> result = new ArrayList<>();
        result.addAll(playersRestSelection.keySet());

        for (String playerName: playersGKsSelection.keySet()) {
            result.remove(playerName);
            result.add(playerName + ",G");
        }

        Utils.sortStringArrayList(result);
        return result;
    }
}