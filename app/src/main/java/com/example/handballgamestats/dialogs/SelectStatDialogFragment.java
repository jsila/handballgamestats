package com.example.handballgamestats.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import com.example.handballgamestats.R;

public class SelectStatDialogFragment extends DialogFragment {
    public interface SelectStatDialogListener {
        void onSelectStatClick(String playerName, boolean isGK, int statId, int amount);
    }

    SelectStatDialogListener listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (SelectStatDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement NoticeDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle args = getArguments();
        final boolean isGKDialog = args.getBoolean("isGKDialog");
        final boolean isGKPlayer = args.getBoolean("isGKPlayer");
        final String playerName = args.getString("playerName");
        final boolean isAdd = args.getBoolean("isAdd");

        int statsArray = isGKDialog ? R.array.statsGKs : (isGKPlayer ? R.array.statsRestGKs : R.array.statsRest);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(String.format("%s %s", isAdd ? "Dodaj k" : "Odvzemi od", playerName))
                .setItems(getResources().getStringArray(statsArray), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int statId) {
                        // statId + 1 because stats are indexed from 1 not 0
                        statId += 1;

                        // because there are two stats normally between goal- 6m and 7m-
                        if (!isGKDialog && isGKPlayer && (statId == 7 || statId == 8)) {
                            statId += 2;
                        }
                        listener.onSelectStatClick(playerName, isGKDialog, statId, isAdd ? 1 : -1);
                    }
                });
        return builder.create();
    }

}
