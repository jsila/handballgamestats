package com.example.handballgamestats.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import com.example.handballgamestats.R;
import com.example.handballgamestats.activities.PlayerListActivity;

import java.util.ArrayList;

public class ConfirmPlayerSelectionDialogFragment extends DialogFragment {
    public interface ConfirmPlayerSelectionDialogListener {
        void onConfirmGamePlayerSelection(String userTeamName, String otherTeamName, boolean isGuestGame);
        void onConfirmDeletePlayerSelection();
    }

    private ConfirmPlayerSelectionDialogListener listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (ConfirmPlayerSelectionDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException("Listener must implement RemovePlayerDialogListener interface");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle args = getArguments();
        ArrayList<String> players = args.getStringArrayList("players");
        final int action = args.getInt("action");
        String title = "";
        int layoutId = 0;

        switch (action) {
            case PlayerListActivity.CONFIRM_DELETE_SELECTION:
                title = "Potrdi odstranitev";
                layoutId = R.layout.activity_player_list;
                break;
            case PlayerListActivity.CONFIRM_GAME_SELECTION:
                title = "Podatki o novi igri";
                layoutId = R.layout.activity_player_list_with_info;
                break;
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, players);
        View v = getActivity().getLayoutInflater().inflate(layoutId, null);
        ListView listView = (ListView) v.findViewById(R.id.player_listview);
        listView.setAdapter(adapter);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title)
                .setView(v)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        switch (action) {
                            case PlayerListActivity.CONFIRM_GAME_SELECTION:
                                Dialog d = (Dialog) dialogInterface;
                                TextView userTeamName = (TextView) d.findViewById(R.id.user_team_name);
                                TextView otherTeamName = (TextView) d.findViewById(R.id.other_team_name);
                                CheckBox isGuestGame = (CheckBox) d.findViewById(R.id.is_guest_game);
                                listener.onConfirmGamePlayerSelection(
                                        userTeamName.getText().toString().trim(),
                                        otherTeamName.getText().toString().trim(),
                                        isGuestGame.isChecked()
                                );
                                break;
                            case PlayerListActivity.CONFIRM_DELETE_SELECTION:
                                listener.onConfirmDeletePlayerSelection();
                                break;
                        }
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
        return builder.create();
    }
}
