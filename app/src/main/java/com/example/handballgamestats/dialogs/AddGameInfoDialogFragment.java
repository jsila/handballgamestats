package com.example.handballgamestats.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.EditText;

import com.example.handballgamestats.R;
import com.example.handballgamestats.lib.GameInfo;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class AddGameInfoDialogFragment extends DialogFragment {
    public interface AddGameInfoDialogListener {
        void onAddGameInfoDialogClick(GameInfo info);
    }

    AddGameInfoDialogListener listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (AddGameInfoDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement AddGameInfoDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle args = getArguments();

        View v = getActivity().getLayoutInflater().inflate(R.layout.dialog_add_game_info, null);

        final GameInfo info = args.getParcelable(GameInfo.class.getName());
        info.setDate(new SimpleDateFormat("d. M. yyyy").format(Calendar.getInstance().getTime()));

        EditText date = (EditText) v.findViewById(R.id.game_date);
        date.setText(info.getDate());

        EditText otherTeamName = (EditText) v.findViewById(R.id.game_opponent);
        otherTeamName.setText(info.getOtherTeamName());

        EditText score = (EditText) v.findViewById(R.id.game_score);
        score.setText(info.getScore());

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder
                .setTitle("Podatki o tekmi")
                .setView(v)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Dialog d = (Dialog) dialog;

                        EditText date          = (EditText) d.findViewById(R.id.game_date);
                        EditText place         = (EditText) d.findViewById(R.id.game_place);
                        EditText time          = (EditText) d.findViewById(R.id.game_time);
                        EditText otherTeamName = (EditText) d.findViewById(R.id.game_opponent);
                        EditText type          = (EditText) d.findViewById(R.id.game_type);
                        EditText score         = (EditText) d.findViewById(R.id.game_score);
                        EditText duration      = (EditText) d.findViewById(R.id.game_duration);

                        info.setDate(date);
                        info.setPlace(place);
                        info.setTime(time);
                        info.setOtherTeamName(otherTeamName);
                        info.setType(type);
                        info.setScore(score);
                        info.setDuration(duration);

                        listener.onAddGameInfoDialogClick(info);
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        return builder.create();
    }
}