package com.example.handballgamestats.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.widget.CheckBox;
import android.widget.EditText;

import com.example.handballgamestats.R;

public class AddPlayerDialogFragment extends DialogFragment {
    public interface AddPlayerDialogListener {
        void onAddPlayerDialogClick(String name, boolean isGoalkeeper);
    }

    AddPlayerDialogListener listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (AddPlayerDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement CreteListDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder
                .setTitle("Dodaj igralca")
                .setView(R.layout.dialog_add_player)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Dialog d = (Dialog) dialog;
                        EditText editText = (EditText) d.findViewById(R.id.player_fullname);
                        CheckBox checkBox = (CheckBox) d.findViewById(R.id.is_goalkeeper);

                        listener.onAddPlayerDialogClick(editText.getText().toString().trim(), checkBox.isChecked());
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        return builder.create();
    }
}