package com.example.handballgamestats.widgets;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.Gravity;

import com.example.handballgamestats.R;

public class TextView extends android.widget.TextView {
    private boolean isFirstColumn;

    public TextView(Context context) {
        super(context);
    }

    public void markAsFirstColumn() {
        this.isFirstColumn = true;
    }

    public void setText(String s) {
        super.setText(s);
        this.setOtherCommonStuff();

    }

    public void setDoubleText(double d) {
        String s = Double.toString(d);
        this.setText(s);
    }

    public void setIntegerText(int i) {
        String s = Integer.toString(i);
        this.setText(s);
    }

    private void setOtherCommonStuff() {
        this.setTextColor(Color.BLACK);

        int h = getResources().getDimensionPixelOffset(R.dimen.overview_cell_horizontal_padding);
        int v = getResources().getDimensionPixelOffset(R.dimen.overview_cell_vertical_padding);
        this.setPadding(h, v, h, v);

        if (!this.isFirstColumn) {
            this.setGravity(Gravity.CENTER);
        }
    }

    public void setAsBold() {
        setTypeface(getTypeface(), Typeface.BOLD);
    }
}
