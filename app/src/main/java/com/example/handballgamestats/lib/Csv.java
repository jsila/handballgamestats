package com.example.handballgamestats.lib;

import java.util.ArrayList;
import java.util.Map;

public class Csv {

    public static class StartAt {
        private int x;
        private int y;

        public StartAt(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }
    }

    private int sizeX;

    private int sizeY;

    private String[][] result;

    public Csv(int sizeX, int sizeY) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.result = new String[sizeY][sizeX];
    }

    public void addGameInfo(GameInfo info, StartAt startAt) {
        String[][] data = {
            {"Datum", info.getDate()},
            {"Kraj", info.getPlace()},
            {"Ura", info.getTime()},
            {"Nasprotnik", info.getOtherTeamName()},
            {"Vrsta tekme", info.getType()},
            {"Rezultat", info.getScore()},
            {"Čas trajanja", info.getDuration()},
        };
        moveAndAdd(startAt, data);
    }

    public void addPlayersHeader(StartAt startAt) {
        String[][] data = {
            {"nastop", "zap", "priimek in ime", "zadetki", "% šut", "gol +", "gol -", "razmerje", "% igra", "9 +", "9 -", "6 +", "6 -", "PN +", "PN -", "7 +", "7 -", "razmerje", "% 7", "A", "izg", "osv", "7 pr", "7 po", "B", "opomin -", "opomin +", "2 min", "2 min +", "RK", "PR", "INDEX"}
        };
        moveAndAdd(startAt, data);
    }

    public void addGKsHeader(StartAt startAt) {
        String[][] data = {
            {"GOLMANI", "", "", "nastop", "zap", "priimek in ime", "obrambe", "% obramb", "obramba", "gol", "razmerje", "% igra", "9 +", "9 -", "6 +", "6 -", "PN +", "PN -", "7 +", "7 -", "razmerje", "% 7", "A", "izg", "osv", "7 pr", "7 po", "B", "opomin -", "opomin +", "2 min", "2 min +", "RK", "PR", "INDEX"}
        };
        moveAndAdd(startAt, data);
    }

    public void addPayersData(ArrayList<Player> players, StartAt startAt, Map<Long, Player> playersMap) {
        String[][] data = new String[players.size()][];

        for (int j=0; j < data.length; j++) {
            Player player = players.get(j);

            data[j] = new String[]{
                "DA",
                Integer.toString(j + 1),
                player.getName(),
                Integer.toString(player.getScore(Stats.GOAL_PLUS)),
                Double.toString(player.getStats().getGoalRatio()),
                Integer.toString(player.getScore(Stats.GAME_GOAL_PLUS)),
                Integer.toString(player.getScore(Stats.GAME_GOAL_MINUS)),
                player.getStats().getGameGoalsRatioString(),
                Double.toString(player.getStats().getGameGoalsRatio()),
                Integer.toString(player.getStats().getScore(Stats.GOAL_9M_PLUS)),
                Integer.toString(player.getStats().getScore(Stats.GOAL_9M_MINUS)),
                Integer.toString(player.getStats().getScore(Stats.GOAL_6M_PLUS)),
                Integer.toString(player.getStats().getScore(Stats.GOAL_6M_MINUS)),
                Integer.toString(player.getStats().getScore(Stats.GOAL_COUNTERATTACK_PLUS)),
                Integer.toString(player.getStats().getScore(Stats.GOAL_COUNTERATTACK_MINUS)),
                Integer.toString(player.getScore(Stats.GOAL_7M_PLUS)),
                Integer.toString(player.getScore(Stats.GOAL_7M_MINUS)),
                player.getStats().get7MGoalsRatioString(),
                Double.toString(player.getStats().get7MGoalsRatio()),
                Integer.toString(player.getScore(Stats.ASSIST)),
                Integer.toString(player.getScore(Stats.BALLS_LOST)),
                Integer.toString(player.getScore(Stats.BALLS_WON)),
                Integer.toString(player.getScore(Stats.BALLS_7M_WON)),
                Integer.toString(player.getScore(Stats.BALLS_7M_GIVEAWAY)),
                Integer.toString(player.getScore(Stats.BLOCK)),
                Integer.toString(player.getScore(Stats.STRIKE)),
                Integer.toString(player.getScore(Stats.STRIKE_PLUS)),
                Integer.toString(player.getScore(Stats.PENALTY_2MIN)),
                Integer.toString(player.getScore(Stats.PENALTY_2MIN_WON)),
                Integer.toString(player.getScore(Stats.RED_CARD)),
                Integer.toString(player.getScore(Stats.FOUL)),
                player.isAssociatedPlayer() ? " " : Integer.toString(player.getStats().getIndex(playersMap)),
            };
        }
        moveAndAdd(startAt, data);
    }

    public void addSummary(String header, ArrayList<Player> players, StartAt startAt) {
        Map<Integer, Integer> scores = Stats.sumScores(players);

        String[][] data = {
            {
                header,
                Integer.toString(scores.get(Stats.GOAL_PLUS)),
                Double.toString(Stats.getGoalRatio(scores)),
                Integer.toString(scores.get(Stats.GAME_GOAL_PLUS)),
                Integer.toString(scores.get(Stats.GAME_GOAL_MINUS)),
                Stats.getGameGoalsRatioString(scores),
                Double.toString(Stats.getGameGoalsRatio(scores)),
                Integer.toString(scores.get(Stats.GOAL_9M_PLUS)),
                Integer.toString(scores.get(Stats.GOAL_9M_MINUS)),
                Integer.toString(scores.get(Stats.GOAL_6M_PLUS)),
                Integer.toString(scores.get(Stats.GOAL_6M_MINUS)),
                Integer.toString(scores.get(Stats.GOAL_COUNTERATTACK_PLUS)),
                Integer.toString(scores.get(Stats.GOAL_COUNTERATTACK_MINUS)),
                Integer.toString(scores.get(Stats.GOAL_7M_PLUS)),
                Integer.toString(scores.get(Stats.GOAL_7M_MINUS)),
                Stats.get7MGoalsRatioString(scores),
                Double.toString(Stats.get7MGoalsRatio(scores)),
                Integer.toString(scores.get(Stats.ASSIST)),
                Integer.toString(scores.get(Stats.BALLS_LOST)),
                Integer.toString(scores.get(Stats.BALLS_WON)),
                Integer.toString(scores.get(Stats.BALLS_7M_WON)),
                Integer.toString(scores.get(Stats.BALLS_7M_GIVEAWAY)),
                Integer.toString(scores.get(Stats.BLOCK)),
                Integer.toString(scores.get(Stats.STRIKE)),
                Integer.toString(scores.get(Stats.STRIKE_PLUS)),
                Integer.toString(scores.get(Stats.PENALTY_2MIN)),
                Integer.toString(scores.get(Stats.PENALTY_2MIN_WON)),
                Integer.toString(scores.get(Stats.RED_CARD)),
                Integer.toString(scores.get(Stats.FOUL)),
                ""
            }
        };
        moveAndAdd(startAt, data);
    }

    public void addGKSPlayersHeader(StartAt startAt) {
        String[][] data = {
                {
                        "GOLMANI",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "zadetki",
                        "% šut",
                        "gol +",
                        "gol -",
                        "9 +",
                        "9 -",
                        "6 +",
                        "6 -",
                        "PN +",
                        "PN -",
                        "razmerje",
                        "% igra",
                        "7 +",
                        "7 -",
                        "razmerje",
                        "% 7"
                }
        };

        moveAndAdd(startAt, data);
    }

    public void addGKSPlayersData(ArrayList<Player> players, StartAt startAt) {
        String[][] data = new String[players.size()][18];

        for (int j=0; j < data.length; j++) {
            Player player = players.get(j);

            data[j] = new String[]{
                    Integer.toString(j+1),
                    "",
                    Integer.toString(player.getScore(Stats.GOAL_PLUS)),
                    Double.toString(player.getStats().getGoalRatio()),
                    Integer.toString(player.getScore(Stats.GAME_GOAL_PLUS)),
                    Integer.toString(player.getScore(Stats.GAME_GOAL_MINUS)),
                    player.getStats().getGameGoalsRatioString(),
                    Double.toString(player.getStats().getGameGoalsRatio()),
                    Integer.toString(player.getStats().getScore(Stats.GOAL_9M_PLUS)),
                    Integer.toString(player.getStats().getScore(Stats.GOAL_9M_MINUS)),
                    Integer.toString(player.getStats().getScore(Stats.GOAL_6M_PLUS)),
                    Integer.toString(player.getStats().getScore(Stats.GOAL_6M_MINUS)),
                    Integer.toString(player.getStats().getScore(Stats.GOAL_COUNTERATTACK_PLUS)),
                    Integer.toString(player.getStats().getScore(Stats.GOAL_COUNTERATTACK_MINUS)),
                    Integer.toString(player.getScore(Stats.GOAL_7M_PLUS)),
                    Integer.toString(player.getScore(Stats.GOAL_7M_MINUS)),
                    player.getStats().get7MGoalsRatioString(),
                    Double.toString(player.getStats().get7MGoalsRatio()),
            };
        }

        moveAndAdd(startAt, data);
    }

    private void moveAndAdd(StartAt startAt, String[][] data) {
        int rows = data[0].length;
        int cols = data.length;

        for (int j = 0; j < cols; j++) {
            for (int i = 0; i < rows; i++) {
                this.result[j+startAt.getY()][i+startAt.getX()] = data[j][i];
            }
        }
    }

    public String getResult() {
        StringBuilder str = new StringBuilder();

        for (int j = 0; j < this.sizeY; j++) {
            for (int i = 0; i < this.sizeX; i++) {
                String value = result[j][i];

                str.append("\"");
                str.append(value == null ? "" : value);
                str.append("\"");

                if (i != this.sizeX-1) str.append(",");
            }
            str.append("\n");
        }
        return str.toString();
    }
}
