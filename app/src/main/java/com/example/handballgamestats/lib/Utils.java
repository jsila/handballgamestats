package com.example.handballgamestats.lib;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;

public class Utils {
    public static void sortStringArrayList(ArrayList<String> items) {
        Collections.sort(items, new Comparator<String>() {
            @Override
            public int compare(String p1, String p2) {
                return p1.compareToIgnoreCase(p2);
            }
        });
    }

    public static void sortPlayerArrayList(ArrayList<Player> items) {
        Collections.sort(items, new Comparator<Player>() {
            @Override
            public int compare(Player p1, Player p2) {
                return p1.getName().compareToIgnoreCase(p2.getName());
            }
        });
    }

    public static void sortPlayersRest(ArrayList<Player> items, final Map<Long,Long> GKsPlayers) {
        Collections.sort(items, new Comparator<Player>() {
            @Override
            public int compare(Player p1, Player p2) {
                String name1 = p1.getName();
                String name2 = p2.getName();
                int gk1 = GKsPlayers.containsValue(p1.getId()) ? 1 : 0;
                int gk2 = GKsPlayers.containsValue(p2.getId()) ? 1 : 0;

                return gk1 == gk2 ? name1.compareToIgnoreCase(name2) : gk1 - gk2;
            }
        });
    }

    public static ArrayList<String> removeDuplicates(ArrayList<String> source, ArrayList<String> duplicates) {
        ArrayList<String> result = new ArrayList<>();

        for(String name: source) {
            if(duplicates.contains(name)) continue;

            result.add(name);
        }

        return result;
    }
}
