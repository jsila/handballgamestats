package com.example.handballgamestats.lib;

import java.util.ArrayList;
import java.util.Map;

public class Players {

    protected Map<Long, Player> playersMap;

    protected Map<Long, Long> GKsPlayers;

    protected ArrayList<Player> playersGKs;

    protected ArrayList<Player> playersRest;

    public Players(StatsDbHelper db) {
        playersMap = db.getPlayersWithStats();
        GKsPlayers = db.getGKsPlayers();

        playersGKs = new ArrayList<>();
        playersRest = new ArrayList<>();

        for(Player player: playersMap.values()) {
            if (player.isGoalkeeper()) {
                playersGKs.add(player);
            } else {
                playersRest.add(player);
            }
        }

        Utils.sortPlayerArrayList(playersGKs);
        Utils.sortPlayersRest(playersRest, GKsPlayers);
    }

    public Map<Long, Player> getPlayersMap() {
        return playersMap;
    }

    public Map<Long, Long> getGKsPlayers() {
        return GKsPlayers;
    }

    public ArrayList<Player> getPlayersGKs() {
        return playersGKs;
    }

    public ArrayList<Player> getPlayersRest() {
        return playersRest;
    }

    public int GKsPlayersQTY() {
        return GKsPlayers.size();
    }

    public int playersRestQTY() {
        return playersRest.size();
    }

    public int playersGKsQTY() {
        return playersGKs.size();
    }
}
