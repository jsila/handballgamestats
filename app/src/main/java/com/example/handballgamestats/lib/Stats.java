package com.example.handballgamestats.lib;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Stats {
    public static final int GOAL_COUNTERATTACK_PLUS = 1;
    public static final int GOAL_COUNTERATTACK_MINUS = 2;

    public static final int GOAL_9M_PLUS = 3;
    public static final int GOAL_9M_MINUS = 4;

    public static final int GOAL_6M_PLUS = 5;
    public static final int GOAL_6M_MINUS = 6;

    public static final int BALLS_LOST = 7;
    public static final int BALLS_WON = 8;

    public static final int GOAL_7M_PLUS = 9;
    public static final int GOAL_7M_MINUS = 10;
    public static final int BALLS_7M_WON = 11;
    public static final int BALLS_7M_GIVEAWAY = 12;

    public static final int STRIKE = 13;
    public static final int STRIKE_PLUS = 14;

    public static final int RED_CARD = 15;

    public static final int PENALTY_2MIN = 16;
    public static final int PENALTY_2MIN_WON = 17;

    public static final int ASSIST = 18;
    public static final int BLOCK = 19;
    public static final int FOUL = 20;

    // computed - its important all clickable stats are above
    public static final int GOAL_PLUS = 21;
    public static final int GOAL_MINUS = 22;
    public static final int GAME_GOAL_PLUS = 23;
    public static final int GAME_GOAL_MINUS = 24;

    public static final int INDEX = 25;

    public static final int[] all = {
        GOAL_COUNTERATTACK_PLUS,
        GOAL_COUNTERATTACK_MINUS,
        GOAL_9M_PLUS,
        GOAL_9M_MINUS,
        GOAL_6M_PLUS,
        GOAL_6M_MINUS,
        BALLS_LOST,
        BALLS_WON,
        GOAL_7M_PLUS,
        GOAL_7M_MINUS,
        BALLS_7M_WON,
        BALLS_7M_GIVEAWAY,
        STRIKE,
        STRIKE_PLUS,
        RED_CARD,
        PENALTY_2MIN,
        PENALTY_2MIN_WON,
        ASSIST,
        BLOCK,
        FOUL,
        GOAL_PLUS,
        GOAL_MINUS,
        GAME_GOAL_PLUS,
        GAME_GOAL_MINUS
    };

    private Map<Integer, Integer> scores;

    private Player player;

    public Stats(Player player) {
        this.player = player;

        scores = new HashMap<>();
        for(int stat: all) {
            scores.put(stat, 0);
        }
    }

    public int getScore(int statId) {
        return this.scores.get(statId);
    }

    public int getIndex(Map<Long, Player> players) {
        int index;

        if (player.isGoalkeeper()) {
            Player associatedPlayer = players.get(player.getAssociatedPlayerId());
            index = 3 * (this.scores.get(GAME_GOAL_PLUS) +  this.scores.get(GOAL_7M_PLUS))
                    + 2 * (associatedPlayer.getScore(GAME_GOAL_MINUS) +  associatedPlayer.getScore(GOAL_7M_PLUS))
                    + this.scores.get(ASSIST)
                    + this.scores.get(BALLS_WON)
                    + this.scores.get(BALLS_7M_WON)
                    + this.scores.get(BLOCK)
                    + this.scores.get(FOUL)
                    + this.scores.get(PENALTY_2MIN_WON)
                    + this.scores.get(STRIKE_PLUS)
                    - this.scores.get(GAME_GOAL_MINUS)
                    - this.scores.get(BALLS_LOST)
                    - 2 * this.scores.get(GOAL_7M_MINUS)
                    - this.scores.get(STRIKE)
                    - this.scores.get(PENALTY_2MIN)
                    - this.scores.get(RED_CARD)
                    - this.scores.get(BALLS_7M_GIVEAWAY)
                    - associatedPlayer.getScore(GOAL_7M_MINUS);
        } else {
            index = 2 * this.scores.get(GOAL_PLUS)
                    + this.scores.get(ASSIST)
                    + this.scores.get(BALLS_WON)
                    + this.scores.get(BALLS_7M_WON)
                    + this.scores.get(BLOCK)
                    + this.scores.get(FOUL)
                    + this.scores.get(PENALTY_2MIN_WON)
                    + this.scores.get(STRIKE_PLUS)
                    - this.scores.get(GAME_GOAL_MINUS)
                    - this.scores.get(BALLS_LOST)
                    - this.scores.get(GOAL_7M_MINUS)
                    - this.scores.get(STRIKE)
                    - this.scores.get(PENALTY_2MIN)
                    - this.scores.get(BALLS_7M_GIVEAWAY)
                    - this.scores.get(RED_CARD);
        }
        return index;
    }

    public void setScore(int statId, int score) {
        this.scores.put(statId, score);
        this.setGoalsPlus();
        this.setGoalsMinus();
        this.setGameGoalsPlus();
        this.setGameGoalsMinus();
    }

    private void setGoalsPlus() {
        int g1 = this.scores.get(GOAL_COUNTERATTACK_PLUS);
        int g2 = this.scores.get(GOAL_6M_PLUS);
        int g3 = this.scores.get(GOAL_9M_PLUS);
        int g4 = this.scores.get(GOAL_7M_PLUS);
        int g = g1 + g2 + g3 + g4;
        this.scores.put(GOAL_PLUS, g);
    }

    private void setGoalsMinus() {
        int g1 = this.scores.get(GOAL_COUNTERATTACK_MINUS);
        int g2 = this.scores.get(GOAL_6M_MINUS);
        int g3 = this.scores.get(GOAL_9M_MINUS);
        int g4 = this.scores.get(GOAL_7M_MINUS);
        int g = g1 + g2 + g3 + g4;
        this.scores.put(GOAL_MINUS, g);
    }

    private void setGameGoalsPlus() {
        int g1 = this.scores.get(GOAL_COUNTERATTACK_PLUS);
        int g2 = this.scores.get(GOAL_6M_PLUS);
        int g3 = this.scores.get(GOAL_9M_PLUS);
        int g = g1 + g2 + g3;
        this.scores.put(GAME_GOAL_PLUS, g);
    }

    private void setGameGoalsMinus() {
        int g1 = this.scores.get(GOAL_COUNTERATTACK_MINUS);
        int g2 = this.scores.get(GOAL_6M_MINUS);
        int g3 = this.scores.get(GOAL_9M_MINUS);
        int g = g1 + g2 + g3;
        this.scores.put(GAME_GOAL_MINUS, g);
    }

    public double getGoalRatio() {
        int scoredAndMissed = this.scores.get(GOAL_PLUS) + this.scores.get(GOAL_MINUS);

        return scoredAndMissed != 0 ? this.scores.get(GOAL_PLUS) * 1.0 / scoredAndMissed : 0;
    }

    public String getGameGoalsRatioString() {
        return this.scores.get(GAME_GOAL_PLUS) + "/" + (this.scores.get(GAME_GOAL_PLUS) + this.scores.get(GAME_GOAL_MINUS));
    }

    public double getGameGoalsRatio() {
        int scoredAndMissed = this.scores.get(GAME_GOAL_PLUS) + this.scores.get(GAME_GOAL_MINUS);

        return scoredAndMissed != 0 ? this.scores.get(GAME_GOAL_PLUS) * 1.0 / scoredAndMissed : 0;
    }

    public String get7MGoalsRatioString() {
        return this.scores.get(GOAL_7M_PLUS) + "/" + (this.scores.get(GOAL_7M_PLUS) + this.scores.get(GOAL_7M_MINUS));
    }

    public double get7MGoalsRatio() {
        int scoredAndMissed = this.scores.get(GOAL_7M_PLUS) + this.scores.get(GOAL_7M_MINUS);

        return scoredAndMissed != 0 ? this.scores.get(GOAL_7M_PLUS) * 1.0 / scoredAndMissed : 0;
    }

    public static Map<Integer, Integer> sumScores(ArrayList<Player> players) {
        Map<Integer, Integer> sums = new HashMap<>();
        for (int statId: all) {
            int res = 0;
            for (Player player: players) {
                res += player.getScore(statId);
            }
            sums.put(statId, res);
        }
        return sums;
    }

    public static double getGoalRatio(Map<Integer, Integer> scores) {
        int scoredAndMissed = scores.get(GOAL_PLUS) + scores.get(GOAL_MINUS);

        return scoredAndMissed != 0 ? scores.get(GOAL_PLUS) * 1.0 / scoredAndMissed : 0;

    }

    public static String getGameGoalsRatioString(Map<Integer, Integer> scores) {
        return scores.get(GAME_GOAL_PLUS) + "/" + (scores.get(GAME_GOAL_PLUS) + scores.get(GAME_GOAL_MINUS));
    }

    public static double getGameGoalsRatio(Map<Integer, Integer> scores) {
        int scoredAndMissed = scores.get(GAME_GOAL_PLUS) + scores.get(GAME_GOAL_MINUS);

        return scoredAndMissed != 0 ? scores.get(GAME_GOAL_PLUS) * 1.0/ scoredAndMissed : 0;
    }

    public static String get7MGoalsRatioString(Map<Integer, Integer> scores) {
        return scores.get(GOAL_7M_PLUS) + "/" + (scores.get(GOAL_7M_PLUS) + scores.get(GOAL_7M_MINUS));
    }

    public static double get7MGoalsRatio(Map<Integer, Integer> scores) {
        int scoredAndMissed = scores.get(GOAL_7M_PLUS) + scores.get(GOAL_7M_MINUS);

        return scoredAndMissed != 0 ? scores.get(GOAL_7M_PLUS) / scoredAndMissed : 0;
    }

    public static int getGoalsFor(Map<Long,Player> players) {
        int goals = 0;
        for (Player player: players.values()) {
            if (player.isGoalkeeper()) continue;
            goals += player.getScore(Stats.GOAL_PLUS);
        }
        return goals;
    }

    public static int getGoalsAgainst(Map<Long,Player> players) {
        int goals = 0;
        for (Player player: players.values()) {
            if (!player.isGoalkeeper()) continue;
            goals += player.getScore(Stats.GOAL_MINUS);
        }
        return goals;
    }
}
