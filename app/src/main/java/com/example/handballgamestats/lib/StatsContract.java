package com.example.handballgamestats.lib;

import android.provider.BaseColumns;

public final class StatsContract {
    private StatsContract() {}

    public static class Players implements BaseColumns {
        public static final String TABLE_NAME = "players";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_IS_GOALKEEPER = "is_goalkeeper";
        public static final String COLUMN_NAME_ASSOCIATED_PLAYER = "associated_player";
    }

    public static class PlayersStats implements BaseColumns {
        public static final String TABLE_NAME = "players_stats";
        public static final String COLUMN_NAME_PLAYERS_ID = "players_id";
        public static final String COLUMN_NAME_STATS_ID = "stats_id";
        public static final String COLUMN_NAME_SCORE = "score";
    }

    public static class PlayersGKs {
        public static final String TABLE_NAME = "players_gks";
        public static final String COLUMN_NAME_PLAYERS_ID = "players_id";
        public static final String COLUMN_NAME_GKS_ID = "gks_id";
    }

    public static class GameInfo {
        public static final String TABLE_NAME = "game_info";
        public static final String COLUMN_NAME_KEY = "key";
        public static final String COLUMN_NAME_VALUE = "value";
    }
}
