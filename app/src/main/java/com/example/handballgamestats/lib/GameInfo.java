package com.example.handballgamestats.lib;

import android.os.Parcel;
import android.os.Parcelable;
import android.widget.EditText;

public class GameInfo implements Parcelable {
    private String date;
    private String place;
    private String time;
    private String type;
    private String score;
    private String duration;
    private String userTeamName;
    private String otherTeamName;
    private boolean isGuestGame;

    public static final String USER_TEAM_NAME = "USER_TEAM_NAME";
    public static final String OTHER_TEAM_NAME = "OTHER_TEAM_NAME";
    public static final String IS_GUEST_GAME = "IS_GUEST_GAME";
    public static final String SCORE = "SCORE";

    public GameInfo() {}

    public void setDate(String date) {
        this.date = date;
    }

    public void setDate(EditText date) {
    this.date = date.getText().toString();
}

    public void setPlace(String place) {
        this.place = place;
    }
    public void setPlace(EditText place) {
        this.place = place.getText().toString();
    }

    public void setTime(String time) {
        this.time = time;
    }
    public void setTime(EditText time) {
        this.time = time.getText().toString();
    }

    public void setType(String type) {
        this.type = type;
    }
    public void setType(EditText type) {
        this.type = type.getText().toString();
    }

    public void setScore(int goalsFor, int goalsAgainst) {
        this.score = isGuestGame
            ? Integer.toString(goalsAgainst) + " : " + Integer.toString(goalsFor)
            : Integer.toString(goalsFor) + " : " + Integer.toString(goalsAgainst);
    }

    public void setScore(String score) {
        this.score = score;
    }
    public void setScore(EditText score) {
        this.score = score.getText().toString();
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
    public void setDuration(EditText duration) {
        this.duration = duration.getText().toString();
    }

    public String getUserTeamName() {
        return userTeamName;
    }

    public void setUserTeamName(String userTeamName) {
        this.userTeamName = userTeamName;
    }

    public String getOtherTeamName() {
        return otherTeamName;
    }

    public void setOtherTeamName(String otherTeamName) {
        this.otherTeamName = otherTeamName;
    }
    public void setOtherTeamName(EditText otherTeamName) {
        this.otherTeamName = otherTeamName.getText().toString();
    }

    public boolean isGuestGame() {
        return isGuestGame;
    }

    public void setGuestGame(boolean guestGame) {
        isGuestGame = guestGame;
    }

    public String getDate() {
        return date;
    }

    public String getPlace() {
        return place;
    }

    public String getTime() {
        return time;
    }

    public String getType() {
        return type;
    }

    public String getScore() {
        return score;
    }

    public String getDuration() {
        return duration;
    }

    public String getGameScoreText() {
        return isGuestGame ? otherTeamName + " : " + userTeamName : userTeamName + " : " + otherTeamName;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.date);
        dest.writeString(this.place);
        dest.writeString(this.time);
        dest.writeString(this.type);
        dest.writeString(this.score);
        dest.writeString(this.duration);
        dest.writeString(this.userTeamName);
        dest.writeString(this.otherTeamName);
        dest.writeByte(this.isGuestGame ? (byte) 1 : (byte) 0);
    }

    protected GameInfo(Parcel in) {
        this.date = in.readString();
        this.place = in.readString();
        this.time = in.readString();
        this.type = in.readString();
        this.score = in.readString();
        this.duration = in.readString();
        this.userTeamName = in.readString();
        this.otherTeamName = in.readString();
        this.isGuestGame = in.readByte() != 0;
    }

    public static final Parcelable.Creator<GameInfo> CREATOR = new Parcelable.Creator<GameInfo>() {
        @Override
        public GameInfo createFromParcel(Parcel source) {
            return new GameInfo(source);
        }

        @Override
        public GameInfo[] newArray(int size) {
            return new GameInfo[size];
        }
    };
}
