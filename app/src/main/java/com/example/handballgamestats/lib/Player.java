package com.example.handballgamestats.lib;

public class Player {

    private long id;

    private String name;

    private Stats stats;

    private boolean goalkeeper = false;

    private long associatedPlayerId = 0;

    private long associatedGKId = 0;

    public Player(String name) {
        this.name = name;
        this.stats = new Stats(this);
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public boolean isGoalkeeper() {
        return this.goalkeeper;
    }

    public int getScore(int statId) {
        return this.stats.getScore(statId);
    }

    public void setGoalkeeper(int goalkeeper) {
        this.goalkeeper = goalkeeper == 1;
    }

    public void setScore(int statId, int score) {
        this.stats.setScore(statId, score);
    }

    public Stats getStats() {
        return this.stats;
    }

    public boolean isAssociatedPlayer() {
        return associatedGKId != 0;
    }

    public void setAssociatedPlayerId(long id) {
        this.associatedPlayerId = id;
    }

    public void setAssociatedGKId(long id) {
        this.associatedGKId = id;
    }

    public long getAssociatedPlayerId() {
        return associatedPlayerId;
    }
}
