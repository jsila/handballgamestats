package com.example.handballgamestats.lib;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class StatsDbHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Stats.db";

    private static final String SQL_CREATE_PLAYERS_TABLE = String.format(
        "CREATE TABLE %s (%s INTEGER PRIMARY KEY, %s STRING, %s INTEGER)",
        StatsContract.Players.TABLE_NAME,
        StatsContract.Players._ID,
        StatsContract.Players.COLUMN_NAME_NAME,
        StatsContract.Players.COLUMN_NAME_IS_GOALKEEPER
    );

    private static final String SQL_CREATE_PLAYERS_STATS_TABLE = String.format(
        "CREATE TABLE %s (%s INTEGER PRIMARY KEY, %s INTEGER, %s INTEGER, %s INTEGER)",
        StatsContract.PlayersStats.TABLE_NAME,
        StatsContract.PlayersStats._ID,
        StatsContract.PlayersStats.COLUMN_NAME_PLAYERS_ID,
        StatsContract.PlayersStats.COLUMN_NAME_STATS_ID,
        StatsContract.PlayersStats.COLUMN_NAME_SCORE
    );

    private static final String SQL_CREATE_PLAYERS_GKS_TABLE = String.format(
            "CREATE TABLE %s (%s INTEGER, %s INTEGER)",
            StatsContract.PlayersGKs.TABLE_NAME,
            StatsContract.PlayersGKs.COLUMN_NAME_GKS_ID,
            StatsContract.PlayersGKs.COLUMN_NAME_PLAYERS_ID
    );

    private static final String SQL_CREATE_UNIQUE_INDEX_PLAYER_STAT = String.format(
        "CREATE UNIQUE INDEX player_stat_idx ON %s(%s, %s)",
        StatsContract.PlayersStats.TABLE_NAME,
        StatsContract.PlayersStats.COLUMN_NAME_PLAYERS_ID,
        StatsContract.PlayersStats.COLUMN_NAME_STATS_ID
    );

    private static final String SQL_CREATE_MATCH_INFO_TABLE = String.format(
            "CREATE TABLE %s (%s STRING, %s STRING)",
            StatsContract.GameInfo.TABLE_NAME,
            StatsContract.GameInfo.COLUMN_NAME_KEY,
            StatsContract.GameInfo.COLUMN_NAME_VALUE
    );

    private static final String SQL_DROP_INDEX_PLAYER_STAT = "DROP INDEX player_stat_idx";

    private static final String SQL_DROP_PLAYERS_TABLE = String.format(
        "DROP TABLE IF EXISTS %s",
        StatsContract.Players.TABLE_NAME
    );

    private static final String SQL_DROP_PLAYERS_STATS_TABLE = String.format(
            "DROP TABLE IF EXISTS %s",
            StatsContract.PlayersStats.TABLE_NAME
    );

    private static final String SQL_DROP_PLAYERS_GKS_TABLE = String.format(
            "DROP TABLE IF EXISTS %s",
            StatsContract.PlayersGKs.TABLE_NAME
    );

    private static String SQL_DROP_MATCH_INFO_TABLE = String.format(
            "DROP TABLE IF EXISTS %s",
            StatsContract.GameInfo.TABLE_NAME
    );

    private static final int GK = 1;
    private static final int NOT_GK = 0;

    private static StatsDbHelper sInstance;

    public static synchronized StatsDbHelper getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new StatsDbHelper(context.getApplicationContext());
        }
        return sInstance;
    }

    public StatsDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_PLAYERS_TABLE);
        db.execSQL(SQL_CREATE_PLAYERS_STATS_TABLE);
        db.execSQL(SQL_CREATE_PLAYERS_GKS_TABLE);
        db.execSQL(SQL_CREATE_UNIQUE_INDEX_PLAYER_STAT);
        db.execSQL(SQL_CREATE_MATCH_INFO_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        recreateDB();
    }

    public void recreateDB() {
        SQLiteDatabase db = this.getWritableDatabase();
        dropDB();
        this.onCreate(db);
    }

    public void dropDB() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(SQL_DROP_INDEX_PLAYER_STAT);
        db.execSQL(SQL_DROP_PLAYERS_TABLE);
        db.execSQL(SQL_DROP_PLAYERS_STATS_TABLE);
        db.execSQL(SQL_DROP_PLAYERS_GKS_TABLE);
        db.execSQL(SQL_DROP_MATCH_INFO_TABLE);
    }

    private Cursor getPlayersCursor() {
        String[] columns = {
            StatsContract.Players._ID,
            StatsContract.Players.COLUMN_NAME_NAME,
            StatsContract.Players.COLUMN_NAME_IS_GOALKEEPER
        };

        SQLiteDatabase db = this.getReadableDatabase();

        return db.query(StatsContract.Players.TABLE_NAME, columns, null, null, null, null, null);
    }

    private Cursor getPlayersGKsCursor() {
        String[] columns = {
                StatsContract.PlayersGKs.COLUMN_NAME_GKS_ID,
                StatsContract.PlayersGKs.COLUMN_NAME_PLAYERS_ID
        };

        SQLiteDatabase db = this.getReadableDatabase();

        return db.query(StatsContract.PlayersGKs.TABLE_NAME, columns, null, null, null, null, null);
    }

    private Cursor getMatchInfoCursor() {
        String[] columns = {
                StatsContract.GameInfo.COLUMN_NAME_KEY,
                StatsContract.GameInfo.COLUMN_NAME_VALUE
        };

        SQLiteDatabase db = this.getReadableDatabase();

        return db.query(StatsContract.GameInfo.TABLE_NAME, columns, null, null, null, null, null);
    }

    private Cursor getStatsCursor() {
        String[] columns = {
            StatsContract.PlayersStats.COLUMN_NAME_PLAYERS_ID,
            StatsContract.PlayersStats.COLUMN_NAME_STATS_ID,
            StatsContract.PlayersStats.COLUMN_NAME_SCORE
        };
        SQLiteDatabase db = this.getReadableDatabase();
        return db.query(StatsContract.PlayersStats.TABLE_NAME, columns, null, null, null, null, null);
    }

    public Map<Long,Player> getPlayersWithStats() {
        Map<Long, Player> players = new HashMap<>();

        Cursor playersCursor = this.getPlayersCursor();

        try {
            if (playersCursor.moveToFirst()) {
                do{
                    long id = playersCursor.getLong(playersCursor.getColumnIndex(StatsContract.Players._ID));
                    String name = playersCursor.getString(playersCursor.getColumnIndex(StatsContract.Players.COLUMN_NAME_NAME));
                    int isGoalkeeper = playersCursor.getInt(playersCursor.getColumnIndex(StatsContract.Players.COLUMN_NAME_IS_GOALKEEPER));

                    Player player = new Player(name);
                    player.setId(id);
                    player.setGoalkeeper(isGoalkeeper);

                    players.put(id, player);
                } while (playersCursor.moveToNext());
            }
        } finally {
            playersCursor.close();
        }

        Map<Long, Long> GKsPlayers = getGKsPlayers();

        for(long gkId: GKsPlayers.keySet()) {
            long playerId = GKsPlayers.get(gkId);
            players.get(gkId).setAssociatedPlayerId(playerId);
            players.get(playerId).setAssociatedGKId(gkId);
        }

        Cursor statsCursor = this.getStatsCursor();

        try {
            if (statsCursor.moveToFirst()) {

                do{
                    long playerId = statsCursor.getLong(statsCursor.getColumnIndex(StatsContract.PlayersStats.COLUMN_NAME_PLAYERS_ID));
                    int statId    = statsCursor.getInt(statsCursor.getColumnIndex(StatsContract.PlayersStats.COLUMN_NAME_STATS_ID));
                    int score     = statsCursor.getInt(statsCursor.getColumnIndex(StatsContract.PlayersStats.COLUMN_NAME_SCORE));

                    players.get(playerId).setScore(statId, score);
                } while (statsCursor.moveToNext());
            }
        } finally {
            statsCursor.close();
        }

        return players;
    }

    public Map<Long, Long> getGKsPlayers() {
        Map<Long,Long> playerGKs = new HashMap<>();
        Cursor playersGKsCursor = this.getPlayersGKsCursor();

        try {
            if (playersGKsCursor.moveToFirst()) {
                do {
                    long playersId = playersGKsCursor.getLong(playersGKsCursor.getColumnIndex(StatsContract.PlayersGKs.COLUMN_NAME_PLAYERS_ID));
                    long gksId = playersGKsCursor.getLong(playersGKsCursor.getColumnIndex(StatsContract.PlayersGKs.COLUMN_NAME_GKS_ID));
                    playerGKs.put(gksId, playersId);
                } while(playersGKsCursor.moveToNext());
            }
        } finally {
            playersGKsCursor.close();
        }

        return playerGKs;

    }

    public boolean updateStat(int statId, Player player) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(StatsContract.PlayersStats.COLUMN_NAME_PLAYERS_ID, player.getId());
        values.put(StatsContract.PlayersStats.COLUMN_NAME_SCORE, player.getScore(statId));
        values.put(StatsContract.PlayersStats.COLUMN_NAME_STATS_ID, statId);

        db.insertWithOnConflict (
            StatsContract.PlayersStats.TABLE_NAME,
            null,
            values,
            SQLiteDatabase.CONFLICT_REPLACE
        );
        return true;
    }

    public void savePlayers(ArrayList<String> gks, ArrayList<String> rest) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();

        try {
            for (String playerName: gks) {
                long gkId = savePlayer(playerName, GK);
                long playersId = savePlayer(playerName, NOT_GK);
                saveGKsPlayers(gkId, playersId);
            }

            for (String playerName: rest) {
                savePlayer(playerName, NOT_GK);
            }

            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    private void saveGKsPlayers(long gkId, long playersId) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(StatsContract.PlayersGKs.COLUMN_NAME_GKS_ID, gkId);
        values.put(StatsContract.PlayersGKs.COLUMN_NAME_PLAYERS_ID, playersId);

        db.insertOrThrow(StatsContract.PlayersGKs.TABLE_NAME, null, values);
    }

    private long savePlayer(String playerName, int isGK) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(StatsContract.Players.COLUMN_NAME_NAME, playerName);
        values.put(StatsContract.Players.COLUMN_NAME_IS_GOALKEEPER, isGK);

        return db.insertOrThrow(StatsContract.Players.TABLE_NAME, null, values);
    }

    public void saveInfo(String userTeamName, String otherTeamName, boolean isGuestGame) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.beginTransaction();

        try {
            ContentValues values = new ContentValues();
            values.put(StatsContract.GameInfo.COLUMN_NAME_KEY, GameInfo.USER_TEAM_NAME);
            values.put(StatsContract.GameInfo.COLUMN_NAME_VALUE, userTeamName);
            db.insertOrThrow(StatsContract.GameInfo.TABLE_NAME, null, values);

            values = new ContentValues();
            values.put(StatsContract.GameInfo.COLUMN_NAME_KEY, GameInfo.OTHER_TEAM_NAME);
            values.put(StatsContract.GameInfo.COLUMN_NAME_VALUE, otherTeamName);
            db.insertOrThrow(StatsContract.GameInfo.TABLE_NAME, null, values);

            values = new ContentValues();
            values.put(StatsContract.GameInfo.COLUMN_NAME_KEY, GameInfo.IS_GUEST_GAME);
            values.put(StatsContract.GameInfo.COLUMN_NAME_VALUE, isGuestGame);
            db.insertOrThrow(StatsContract.GameInfo.TABLE_NAME, null, values);

            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    public GameInfo getGameInfo() {
        GameInfo info = new GameInfo();

        Cursor c = getMatchInfoCursor();

        try {
            if (c.moveToFirst()) {

                do{
                    String key = c.getString(c.getColumnIndex(StatsContract.GameInfo.COLUMN_NAME_KEY));
                    String value = c.getString(c.getColumnIndex(StatsContract.GameInfo.COLUMN_NAME_VALUE));

                    switch (key) {
                        case GameInfo.USER_TEAM_NAME:
                            info.setUserTeamName(value);
                            break;
                        case GameInfo.OTHER_TEAM_NAME:
                            info.setOtherTeamName(value);
                            break;
                        case GameInfo.IS_GUEST_GAME:
                            info.setGuestGame(value.equals("1"));
                            break;
                    }

                } while (c.moveToNext());
            }
        } finally {
            c.close();
        }

        return info;
    }
}
